import settings
from os import getenv
from game import Game
BOT_TOKEN = getenv('BOT_TOKEN')

import discord
intents = discord.Intents.default()
intents.members = True
from discord.ext.commands import Bot
mafiabot = Bot(command_prefix='$', intents=intents)

import random

import logging

games = []

@mafiabot.event 
async def on_ready():
    print('Logged on as {0.user}!'.format(mafiabot))

# Register a command
@mafiabot.command()
async def hello(ctx):
    await ctx.send('hello!')
    print('Message from {0.author}: {0.content}'.format(ctx.message))

@mafiabot.command()
async def ping(ctx):
    ms = round(mafiabot.latency*1000)
    await ctx.send(f':ping_pong: Pong! **{ms}ms**')

@mafiabot.command()
async def rand(ctx):
    await ctx.send(random.randint(0,100))

@mafiabot.group()
async def game(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send('Invalid sub command passed')

@game.command()
async def create(ctx):
    # await ctx.guild.create_voice_channel('Town Square')
    game = Game(mafiabot, ctx)
    await game.create()
    games.append(game)
    await ctx.send(f'{ctx.author.mention} created a game. Join it by entering the `{ctx.author.display_name}\'s Mafia Game` voice channel. Start it by typing {mafiabot.command_prefix}game start')


def get_game(ctx):
    return next(filter(lambda game: game.author == ctx.author, games), None)

@game.command()
async def start(ctx, debug: bool=False):
    game = get_game(ctx)
    if game is not None:
        if debug or len(game.members) > 2:
            await game.start()
            await ctx.send("Game started")
        else:
            await ctx.send("There must be at least three players in order to start this game.")
    else:
        await ctx.send("You haven't created a game.")

@game.command()
async def stop(ctx):
    game = get_game(ctx)
    if game is not None:
        await game.stop()
        await ctx.send("Game stopped")
    else:
        await ctx.send("You haven't created a game.")



@mafiabot.event
async def on_message(message):
    if message.author == mafiabot.user:
        return

    # Process the commands using Discord's built in system 
    # or, eventually, if you don't need it, you can delete
    # on_message entirely
    await mafiabot.process_commands(message)

mafiabot.run(BOT_TOKEN)
