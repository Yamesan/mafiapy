import discord
from random import sample

class Game:
    def __init__(self, bot, ctx):
        self.bot = bot
        self.guild = ctx.guild
        self.author = ctx.author
        self.members = []

    # chooses the detective
    def choose_mafias(self):
        # hardcoded to one for now
        n = 1
        self.mafias = sample(self.members, n)

    def choose_detectives(self):
        # also hardcoded for testing
        n = 1
        self.detectives = sample(self.members, n)

    async def create(self):
        overwrites = {
            self.guild.default_role: discord.PermissionOverwrite(speak=False)
        }
        self.voice_channel = await self.guild.create_voice_channel(f"{self.author.display_name}'s Mafia Game", user_limit=12, overwrites=overwrites)
        return self
        
    async def start(self):
        # Set game members to those in the voice channel
        print(self.voice_channel, self.voice_channel.members)
        self.members = list(filter(lambda member: member.id != self.bot.user.id, self.voice_channel.members))


        # Create the town square
        # Set perms
        town_square_overwrites = {
            self.guild.default_role: discord.PermissionOverwrite(read_messages=False),
            self.guild.me: discord.PermissionOverwrite(read_messages=True, send_messages=True)
        }
        for member in self.members:
            town_square_overwrites[member] = discord.PermissionOverwrite(read_messages=True, send_messages=True)

        # Create channel with perms
        self.town_square = await self.guild.create_text_channel("town-square", overwrites=town_square_overwrites) 

        # Create mafia channel
        # Set perms
        mafia_bulletin_overwrites = {
            self.guild.default_role: discord.PermissionOverwrite(read_messages=False),
            self.guild.me: discord.PermissionOverwrite(read_messages=True, send_messages=True),
        }
        self.choose_mafias()
        for member in self.mafias:
            mafia_bulletin_overwrites[member] = discord.PermissionOverwrite(read_messages=True, send_messages=False)
        
        # temporarily adding it here
        self.choose_detectives()

        # Create channel with perms
        self.mafia_bulletin = await self.guild.create_text_channel("mafia-bulletin", overwrites=mafia_bulletin_overwrites)

        # Send a message to the town square
        mentions = " ".join([member.mention for member in self.members])
        await self.town_square.send(f"Welcome, {mentions}. Unless you can see the mafia channel, you are a villager.")

        mentions = " ".join([member.mention for member in self.mafias])
        await self.mafia_bulletin.send(f"Hello, {mentions}.")

        # saving the code below
        # detective = " ".join([member.mention for member in self.detectives])

        # this will dm the chosen detective 
        await member.send(f"You are the detectve.")

    async def stop(self):
        # this will remove all active users on the created voice channel 
        for member in self.members:
            await member.edit(voice_channel=None)
        # delete channels
        await self.town_square.delete()
        await self.mafia_bulletin.delete()
